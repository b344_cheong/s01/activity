import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        String firstName, lastName;
        double firstSubject, secondSubject, thirdSubject, avgGrade;

        System.out.print("First Name: ");
        firstName = myObj.nextLine();
        System.out.print("Last Name: ");
        lastName = myObj.nextLine();

        System.out.print("First Subject Grade: ");
        firstSubject = myObj.nextDouble();
        System.out.print("Second Subject Grade: ");
        secondSubject = myObj.nextDouble();
        System.out.print("Third Subject Grade: ");
        thirdSubject = myObj.nextDouble();

        avgGrade = (firstSubject + secondSubject + thirdSubject) / 3;
        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + avgGrade);
    }
}